package com.lowpolybutt.planit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lowpolybutt.planit.models.ListItem

class ListItemRecyclerViewAdapter : RecyclerView.Adapter<ListItemRecyclerViewAdapter.ViewHolder>() {
    private var itemsList: ArrayList<ListItem> = ArrayList()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBox: CheckBox = itemView.findViewById(R.id.list_item_recyclerview_item_layout_is_item_complete)
        val itemTitle: TextView = itemView.findViewById(R.id.list_item_recyclerview_item_layout_item_title)
        val recurrence: TextView = itemView.findViewById(R.id.list_item_recyclerview_item_layout_item_recurrence_label)
    }

    override fun getItemCount(): Int = itemsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.checkBox.isChecked = itemsList[position].bIsComplete
        holder.itemTitle.text = itemsList[position].title
        holder.recurrence.text = itemsList[position].recurrenceType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_recyclerview_item_layout, parent, false)
        return ViewHolder(v)
    }

    fun updateList(list: List<ListItem>) {
        list.forEachIndexed { i, p ->
            if (!itemsList.contains(p)) {
                itemsList.add(p)
                notifyItemInserted(i)
            }
        }
    }
}