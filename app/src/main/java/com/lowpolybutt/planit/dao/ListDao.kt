package com.lowpolybutt.planit.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.lowpolybutt.planit.models.ListItem
import kotlinx.coroutines.flow.Flow

@Dao
interface ListDao {
    @Query("SELECT * from list_items")
    fun getListItems(): LiveData<List<ListItem>>

    @Query("SELECT * FROM list_items WHERE uid = :uid")
    fun getListItemForUid(uid: Long): Flow<ListItem>

    @Query("SELECT * FROM list_items WHERE recurrenceType = (:recurrenceType)")
    fun getItemsForRecurrenceType(vararg recurrenceType: String): LiveData<List<ListItem>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertListItems(vararg listItems: ListItem)

    @Update
    suspend fun updateListItems(vararg listItems: ListItem)

    @Query("DELETE FROM list_items")
    suspend fun deleteAll()

    @Query("DELETE FROM list_items WHERE uid = :uid")
    suspend fun deleteItemForUid(uid: Long)
}