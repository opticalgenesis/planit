package com.lowpolybutt.planit.fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.lowpolybutt.planit.AppDatabase
import com.lowpolybutt.planit.R
import com.lowpolybutt.planit.models.ListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewListItemDialog : DialogFragment() {

    private lateinit var title: String
    private lateinit var fullDesc: String

    private val db by lazy { AppDatabase.getInstance(requireContext()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = layoutInflater.inflate(R.layout.dialog_new_list_item_layout, null, false)

        val titleInput: TextInputEditText = view.findViewById(R.id.dialog_new_list_item_title_input)
        val fullDescInput: TextInputEditText = view.findViewById(R.id.dialog_new_list_item_full_description_input)

        title = titleInput.editableText.toString()
        fullDesc = fullDescInput.editableText.toString()

        return MaterialAlertDialogBuilder(requireContext())
            .setView(view)
            .setTitle("Enter List Item Details")
            .setPositiveButton("Add") { _, _ ->
                lifecycleScope.launch(Dispatchers.IO) {
                    db.listItemDao().insertListItems(ListItem(
                        title = title,
                        fullDescription = fullDesc,
                        recurrenceType = ListItem.RECURRENCE_TYPE_DAILY,
                        bIsRecurring = false, // make this dynamic from recurrence type
                        bIsComplete = false,
                        bShouldRemind = false,
                        completionTime = 0
                    ))
                }.invokeOnCompletion { Log.d("PLANIT_LOG", "Item added!") }
            }
            .setNegativeButton("Cancel") { _, _ -> dismiss() }.create()
    }
}