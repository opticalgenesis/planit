package com.lowpolybutt.planit.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lowpolybutt.planit.AppDatabase
import com.lowpolybutt.planit.ListItemRecyclerViewAdapter
import com.lowpolybutt.planit.R
import com.lowpolybutt.planit.models.ListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentMain : Fragment() {
    private lateinit var rv: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv = view.findViewById(R.id.fragment_main_list_item_recycler)
        rv.adapter = ListItemRecyclerViewAdapter()
        rv.layoutManager = LinearLayoutManager(requireContext())
        lifecycleScope.launch { getHourlyAndDailyTasks() }.invokeOnCompletion { Log.d("PLANIT_DATA", "Fetched data") }
    }

    private fun getHourlyAndDailyTasks() {
        val db = AppDatabase.getInstance(requireContext())
        db.listItemDao().getItemsForRecurrenceType(ListItem.RECURRENCE_TYPE_DAILY).observe(requireActivity()) {
            (rv.adapter as ListItemRecyclerViewAdapter).updateList(it)
        }
    }
}