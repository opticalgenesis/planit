package com.lowpolybutt.planit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.progressindicator.LinearProgressIndicator
import com.lowpolybutt.planit.fragments.NewListItemDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var progressBar: LinearProgressIndicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNav: BottomNavigationView = findViewById(R.id.activity_main_bottom_nav_view)

        val topAppBar: MaterialToolbar = findViewById(R.id.activity_main_material_toolbar)
        setSupportActionBar(topAppBar)

        val navHostFragment: NavHostFragment = supportFragmentManager.findFragmentById(R.id.activity_main_nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        bottomNav.setupWithNavController(navController)

        bottomNav.setOnItemSelectedListener {
            it.onNavDestinationSelected(navController)
        }

        val newItemButton: ExtendedFloatingActionButton = findViewById(R.id.activity_main_ext_fab_new_item)
        newItemButton.setOnClickListener {
            NewListItemDialog().show(supportFragmentManager, "NEW_LIST_ITEM_DIALOG")
        }

        progressBar = findViewById(R.id.activity_main_progress_indicator)

        setupProgressListener()
    }

    private fun setupProgressListener() {
        val totalItems = AppDatabase.getInstance(this).listItemDao().getListItems()
        totalItems.observe(this) {
            val count = it.size
            var completedCount = 0
            it.forEach { listItem ->
                if (listItem.bIsComplete) {
                    completedCount++
                }
            }
            val completePercent = (count - completedCount) / completedCount * 100
            lifecycleScope.launch(Dispatchers.Main) {
                progressBar.progress = completePercent.toInt()
            }
        }
    }
}