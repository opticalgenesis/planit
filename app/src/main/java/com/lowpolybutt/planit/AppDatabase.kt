package com.lowpolybutt.planit

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lowpolybutt.planit.dao.ListDao
import com.lowpolybutt.planit.models.ListItem

@Database(entities = [ListItem::class], version = 1, exportSchema = true)
abstract class AppDatabase: RoomDatabase() {
    abstract fun listItemDao(): ListDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(ctx: Context) = INSTANCE ?: Room.databaseBuilder(ctx, AppDatabase::class.java, "app-db").fallbackToDestructiveMigration().build().also { INSTANCE = it }
    }
}