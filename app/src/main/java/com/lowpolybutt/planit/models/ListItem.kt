package com.lowpolybutt.planit.models

import androidx.room.Entity
import androidx.room.PrimaryKey

// TODO -- add time to be reminded at
@Entity(tableName = "list_items")
data class ListItem(
    @PrimaryKey(autoGenerate = true) var uid: Long = 0,
    val title: String, // Summary title of the item; "drink 2 L water by end of day" for example
    val fullDescription: String, // Full description of the item
    val recurrenceType: String, // Will be one of NONE, DAILY, WEEKLY, MONTHLY, YEARLY
    val bIsRecurring: Boolean, // If the item should be added to a regular rotation
    val bIsComplete: Boolean, // If the user has completed the item
    val bShouldRemind: Boolean, // If a notification should be sent at a user-defined time before completionTime to remind them
    val completionTime: Long // UNIX time by which the item should be complete,
    ) {

    companion object {
        const val RECURRENCE_TYPE_NONE: String = "NONE"
        const val RECURRENCE_TYPE_DAILY: String = "DAILY"
        const val RECURRENCE_TYPE_WEEKLY: String = "WEEKLY"
        const val RECURRENCE_TYPE_MONTHLY: String = "MONTHLY"
        const val RECURRENCE_TYPE_YEARLY: String = "YEARLY"
    }

    override fun toString(): String {
        return "Title: $title\nFull Description: $fullDescription\nIs Recurring: $bIsRecurring\nIs Complete: $bIsComplete\nShould Remind: $bShouldRemind"
    }
}
